<?php

namespace Tripcloud\AsyncEventDispatcherBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Tripcloud\AsyncEventDispatcherBundle\DependencyInjection\Compiler\ReplaceEventDispatcherPass
 */
class ReplaceEventDispatcherPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (
            !$container->hasParameter('tripcloud.producer.service_id') ||
            !$container->getParameter('tripcloud.producer.service_id')
        ) {
            $container->removeDefinition('tripcloud.async_event_dispatcher');

            return;
        }

        $container->setAlias(
            'tripcloud.event_dispatcher_producer',
            new Alias($container->getParameter('tripcloud.producer.service_id'), false)
        );

        $container->setDefinition(
            'event_dispatcher',
            $container->getDefinition('tripcloud.async_event_dispatcher')
        );
    }
}
