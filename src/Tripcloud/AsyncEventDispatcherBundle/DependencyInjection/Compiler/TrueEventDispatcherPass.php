<?php

namespace Tripcloud\AsyncEventDispatcherBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Tripcloud\AsyncEventDispatcherBundle\DependencyInjection\Compiler\TrueEventDispatcherPass
 */
class TrueEventDispatcherPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (
            !$container->hasParameter('tripcloud.producer.service_id') ||
            !$container->getParameter('tripcloud.producer.service_id')
        ) {
            return;
        }

        if ($container->hasAlias('event_dispatcher')) {
            $container->setAlias(
                'tripcloud.true_event_dispatcher',
                new Alias((string) $container->getAlias('event_dispatcher'), false)
            );
        } else {
            $definition = $container->getDefinition('event_dispatcher');
            $definition->setPublic(false);
            $container->setDefinition('tripcloud.true_event_dispatcher', $definition);
        }
    }
}
