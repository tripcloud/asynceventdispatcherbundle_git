<?php

namespace Tripcloud\AsyncEventDispatcherBundle;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Tripcloud\AsyncEventDispatcherBundle\DependencyInjection\Compiler\ReplaceEventDispatcherPass;
use Tripcloud\AsyncEventDispatcherBundle\DependencyInjection\Compiler\TrueEventDispatcherPass;

/**
 * Tripcloud\AsyncEventDispatcherBundle\TripcloudAsyncEventDispatcherBundle
 */
class TripcloudAsyncEventDispatcherBundle extends Bundle
{
    /**
     * {@inheritdoc}
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new TrueEventDispatcherPass());
        $container->addCompilerPass(new ReplaceEventDispatcherPass());
    }
}
