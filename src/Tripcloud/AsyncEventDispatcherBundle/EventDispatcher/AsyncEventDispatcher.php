<?php

namespace Tripcloud\AsyncEventDispatcherBundle\EventDispatcher;

use OldSound\RabbitMqBundle\RabbitMq\Producer;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Tripcloud\AsyncEventDispatcherBundle\Event\AsyncEvent;

/**
 * Tripcloud\AsyncEventDispatcherBundle\EventDispatcher\AsyncEventDispatcher
 */
class AsyncEventDispatcher implements EventDispatcherInterface
{
    const GZIP_COMPRESSION = 4;

    /**
     * @var Producer
     */
    protected $rabbitMqProducer;

    /**
     * @var EventDispatcherInterface
     */
    protected $trueDispatcher;

    /**
     * Class constructor
     *
     * @param EventDispatcherInterface $trueDispatcher
     * @param Producer                 $rabbitMqProducer
     */
    public function __construct(
        EventDispatcherInterface $trueDispatcher,
        Producer $rabbitMqProducer
    ) {
        $this->trueDispatcher = $trueDispatcher;
        $this->rabbitMqProducer = $rabbitMqProducer;
    }

    /**
     * {@inheritdoc}
     *
     * Lazily loads listeners for this event from the dependency injection
     * container.
     *
     * @throws \InvalidArgumentException if the service is not defined
     */
    public function dispatch($eventName, Event $event = null)
    {
        if ($event instanceof AsyncEvent && !$event->isReDispatched()) {
            return $this->publishEvent($eventName, $event);
        }

        return $this->trueDispatcher->dispatch($eventName, $event);
    }

    /**
     * {@inheritdoc}
     */
    public function addListener($eventName, $listener, $priority = 0)
    {
        return $this->trueDispatcher->addListener($eventName, $listener, $priority);
    }

    /**
     * {@inheritdoc}
     */
    public function addSubscriber(EventSubscriberInterface $subscriber)
    {
        return $this->trueDispatcher->addSubscriber($subscriber);
    }

    /**
     * {@inheritdoc}
     */
    public function removeListener($eventName, $listener)
    {
        return $this->trueDispatcher->removeListener($eventName, $listener);
    }

    /**
     * {@inheritdoc}
     */
    public function removeSubscriber(EventSubscriberInterface $subscriber)
    {
        return $this->trueDispatcher->removeSubscriber($subscriber);
    }

    /**
     * {@inheritdoc}
     */
    public function getListeners($eventName = null)
    {
        return $this->trueDispatcher->getListeners($eventName);
    }

    /**
     * {@inheritdoc}
     */
    public function getListenerPriority($eventName, $listener)
    {
        return $this->trueDispatcher->getListenerPriority($eventName, $listener);
    }

    /**
     * {@inheritdoc}
     */
    public function hasListeners($eventName = null)
    {
        return $this->trueDispatcher->hasListeners($eventName);
    }

    /**
     * @param string $name
     * @param array  $arguments
     *
     * @return mixed
     */
    public function __call($name, $arguments)
    {
        return call_user_func_array(array($this->trueDispatcher, $name), $arguments);
    }

    /**
     * Publish event to queue
     *
     * @param string     $eventName
     * @param AsyncEvent $event
     *
     * @return AsyncEvent
     */
    protected function publishEvent($eventName, AsyncEvent $event)
    {
        $event = clone $event;
        $event->setName($eventName);
        $event->setReDispatched();
        $this->rabbitMqProducer->publish(
            gzcompress(serialize($event), self::GZIP_COMPRESSION),
            $event->getRoutingKey(),
            ['delivery_mode' => $event->getDeliveryMode()]
        );

        return $event;
    }
}
