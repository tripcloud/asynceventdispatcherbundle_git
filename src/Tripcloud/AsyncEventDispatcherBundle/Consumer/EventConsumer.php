<?php

namespace Tripcloud\AsyncEventDispatcherBundle\Consumer;

use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Tripcloud\AsyncEventDispatcherBundle\Event\AsyncEvent;

/**
 * Tripcloud\AsyncEventDispatcherBundle\Consumer\EventConsumer
 */
class EventConsumer implements ConsumerInterface
{
    /**
     * @var EventDispatcherInterface
     */
    protected $eventDispatcher;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * Class constructor
     *
     * @param EventDispatcherInterface $eventDispatcher
     * @param LoggerInterface          $logger
     */
    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        LoggerInterface $logger = null
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $logger;
    }

    /**
     * Execute message from queue
     *
     * @param AMQPMessage $msg
     *
     * @return bool
     * @throws \Exception
     */
    public function execute(AMQPMessage $msg)
    {
        try {
            if ($msg->body[1] !== ':') {
                $msg->body = gzuncompress($msg->body);
            }

            $event = unserialize($msg->body);
            if (!$event instanceof AsyncEvent) {
                throw new \InvalidArgumentException("Event consumer can only redispatch async events");
            }

            $this->eventDispatcher->dispatch($event->getName(), $event);
        } catch (\Exception $exception) {
            if (isset($event) && $event instanceof AsyncEvent && $event->getRetryAttempts() > 0) {
                $event->decreaseRetryAttempts();
                $event->setReDispatched(false);

                if ($this->logger) {
                    $this->logger->warning($exception);
                }

                $this->eventDispatcher->dispatch($event->getName(), $event);
            } else {
                if ($this->logger) {
                    $this->logger->error($exception);
                } else {
                    throw $exception;
                }
            }
        }

        return true;
    }
}
