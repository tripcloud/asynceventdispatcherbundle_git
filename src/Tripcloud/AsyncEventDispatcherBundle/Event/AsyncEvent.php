<?php

namespace Tripcloud\AsyncEventDispatcherBundle\Event;

use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\EventDispatcher\Event;

/**
 * Tripcloud\AsyncEventDispatcherBundle\Event\AsyncEvent
 */
class AsyncEvent extends Event
{
    const DEFAULT_ROUTING_KEY = '';

    /**
     * @var bool
     */
    protected $reDispatch = true;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $routingKey = self::DEFAULT_ROUTING_KEY;

    /**
     * @var int
     */
    protected $deliveryMode = AMQPMessage::DELIVERY_MODE_PERSISTENT;

    /**
     * Retry attempts if failed
     *
     * @var int
     */
    protected $retryAttempts = 0;

    /**
     * Set event as re-dispatched
     *
     * @param bool $reDispatched
     *
     * @return static
     */
    public function setReDispatched($reDispatched = true)
    {
        $this->reDispatch = !$reDispatched;

        return $this;
    }

    /**
     * Check if event has been re-dispatched
     *
     * @return bool
     */
    public function isReDispatched()
    {
        return !$this->reDispatch;
    }

    /**
     * Getter of Name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Setter of Name
     *
     * @param string $name
     *
     * @return static
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Getter of RoutingKey
     *
     * @return string
     */
    public function getRoutingKey()
    {
        return $this->routingKey;
    }

    /**
     * Setter of RoutingKey
     *
     * @param string $routingKey
     *
     * @return static
     */
    public function setRoutingKey($routingKey)
    {
        $this->routingKey = $routingKey;

        return $this;
    }

    /**
     * @return array
     */
    public function __sleep()
    {
        return array_keys(get_object_vars($this));
    }

    /**
     * @return int
     */
    public function getRetryAttempts()
    {
        return $this->retryAttempts;
    }

    /**
     * @param int $retryAttempts
     *
     * @return static
     */
    public function setRetryAttempts($retryAttempts)
    {
        $this->retryAttempts = $retryAttempts;

        return $this;
    }

    /**
     * Getter of DeliveryMode
     *
     * @return int
     */
    public function getDeliveryMode()
    {
        return $this->deliveryMode;
    }

    /**
     * Setter of DeliveryMode
     *
     * @param int $deliveryMode
     *
     * @return static
     */
    public function setDeliveryMode($deliveryMode)
    {
        $this->deliveryMode = (int) $deliveryMode;

        return $this;
    }

    /**
     * @return static
     */
    public function decreaseRetryAttempts()
    {
        $this->retryAttempts--;

        return $this;
    }
}
